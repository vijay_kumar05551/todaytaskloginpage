//
//  ViewController2.m
//  loginPage
//
//  Created by Cli16 on 11/17/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController2.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController2 () <AVAudioPlayerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *nextBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UIButton *playMusicBtn;
@property (strong, nonatomic) IBOutlet UIButton *pauseBtn;

@end

@implementation ViewController2
@synthesize nextBtn;
@synthesize userNameLabel;
@synthesize passwordLabel;
@synthesize playMusicBtn;
@synthesize pauseBtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *doubleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapPressed:)];
    doubleTap.numberOfTapsRequired=2;
    doubleTap.numberOfTouchesRequired=1;
    //  [doubleTap addTarget:self action:@selector(doubleTapPressed:)];
    [self.view addGestureRecognizer:doubleTap];

    // Do any additional setup after loading the view.
}
-(void)doubleTapPressed:(UITapGestureRecognizer *)sender
{
    NSString *userName=[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    userNameLabel.text=[NSString stringWithFormat:@"%@",userName];
    [[NSUserDefaults standardUserDefaults]synchronize];
   
    NSString *password=[[NSUserDefaults standardUserDefaults]objectForKey:@"passward"];
    passwordLabel.text=[NSString stringWithFormat:@"%@",password];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
- (IBAction)nextButtonPressed:(id)sender {
    
    NSString *userName=[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    userNameLabel.text=[NSString stringWithFormat:@"%@",userName];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *password=[[NSUserDefaults standardUserDefaults]objectForKey:@"passward"];
    passwordLabel.text=[NSString stringWithFormat:@"%@",password];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}
   //add music
- (IBAction)playMusicBtnPressed:(id)sender {
    NSString *music=[[NSBundle mainBundle]pathForResource:@"tujo" ofType:@"mp3"];
    audioPlay=[[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL URLWithString:music] error:nil];
    audioPlay.delegate=self;
    audioPlay.numberOfLoops=1;
    [audioPlay play];
    
    
}
- (IBAction)pauseBtnPressed:(id)sender {
    [audioPlay pause];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    }


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
